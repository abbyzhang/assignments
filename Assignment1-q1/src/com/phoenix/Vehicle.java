package com.phoenix;

public class Vehicle {
	private int workId;
	private String technician;
	private String status;
	private int daysInShop;
	private boolean partRequired;
	

	public Vehicle(int workId, String technician, String status, int daysInShop, boolean partRequired) {
		this.workId = workId;
		this.technician = technician;
		this.status = status;
		this.daysInShop = daysInShop;
		this.partRequired = partRequired;
	}
	
	public void start() {
		System.out.println(technician + " is start to work on your vehicle, and tracking Id is: " + workId);
	}
	public String getStatus() {
		return status;
	}

	
	public int getDaysInShop() {
		return daysInShop;
	}

	public void setDaysInShop(int daysInShop) {
		this.daysInShop = daysInShop;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

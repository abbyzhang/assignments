package com.phoenix;

public class Main {

	public static void main(String[] args) {
		//vehicle
		Vehicle myVehicle = new Vehicle(123012, "Tim", "pending", 2, false);
		myVehicle.start();
		
		//customer
		Customer john = new Customer("John", 12, "123-456-7890");
		john.requestService();
		john.enrollment();
		
		//inventory
		Inventory windshield = new Inventory("windshield", 89.0, 5);
		
		//accounting
		Accounting receivePayment = new Accounting("payer");
		receivePayment.setAccountReceivable(120);
		System.out.println("Your account balance is: " + receivePayment.getIncomeBalance() );
	}
}

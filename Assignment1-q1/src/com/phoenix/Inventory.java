package com.phoenix;

public class Inventory {
	private String itemName;
	private double price;
	private int quantity;
	
	public Inventory(String itemName, double price, int quantity) {
		this.itemName = itemName;
		this.price = price;
		this.quantity = quantity;
	}
	
}

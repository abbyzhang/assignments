package com.phoenix;

public class Accounting {
	private double accountPayable;
	private double accountReceivable;
	private double payroll;
	private String name;
	private double incomeBalance;
	
	public Accounting() {
		this(null);
	}
	
	public Accounting(String name) {
		this.accountPayable = 0.0;
		this.accountReceivable = 0.0;
		this.payroll = 0.0;
		this.name = name;
		this.incomeBalance = 0.0;
	}

	public double getAccountReceivable() {
		return accountReceivable;
	}

	public void setAccountReceivable(double accountReceivable) {
		this.accountReceivable -= accountReceivable;
		this.incomeBalance += accountReceivable;
	}

	public double getIncomeBalance() {
		return incomeBalance;
	}	
	

}

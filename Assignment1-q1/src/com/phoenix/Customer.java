package com.phoenix;

public class Customer {
	private String name;
	private int monthAsCustomer;
	private boolean enrollCustomerBenfitProgram;
	private String phone;
	public Customer(String name, int monthAsCustomer, String phone) {
		this.name = name;
		this.monthAsCustomer = monthAsCustomer;
		this.enrollCustomerBenfitProgram = false;
		this.phone = phone;
	}
	
	public void requestService() {
		System.out.println(name + " is requesting service " + " , customer can be reached at: " + phone);
	}
	public void enrollment() {
		if(this.monthAsCustomer >= 24) {
			System.out.println("Welcome, you're now part of customer benfit program");
			this.enrollCustomerBenfitProgram = true;
		}else {
			System.out.println("Sorry, you have to shop with us for 2 years to eligible to enroll the program. You have been shop with us for " + monthAsCustomer + " monthes.");
		}
	}
}

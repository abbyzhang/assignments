
public class Main {
	public static void main(String[] args) {
		//public transport 
		PublicTransport bus = new PublicTransport("bus", "Diesel", 60, "land", 50, 2000, 99099,0);
		bus.stop();
		bus.addOnBoardingPeople(8);
		System.out.println("Add " + bus.getOnboardingPeople() + " passengers"); 
		bus.start();
		bus.move();
		
		//Military vehicle
		MilitaryVehicle rv = new MilitaryVehicle(false);
		rv.setLoadWeapon(true);
		if(rv.isLoadWeapon()) {
			rv.defend();
		}
		if(rv.getMaxSpeed() > 50) {
			rv.attack();
		}
		
		// Personal vehicle
		PersonalVehicle ford = new PersonalVehicle("ford", 12000.0, "gasoline",100, "land", 5, 9000, "Jenny", "red");
		ford.sell();
		
	}
}

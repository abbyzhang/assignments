
public class PersonalVehicle extends CivilianVehicle{
	
	private String ownerName;
	private String color;
	public PersonalVehicle(String name, double cost, String fuel, int maxSpeed, String transportationMedium,
			int capacity, int mileage, String ownerName, String color) {
		super(name, cost, fuel, maxSpeed, transportationMedium, capacity, mileage);
		this.ownerName = ownerName;
		this.color = color;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	@Override
	public void sell() {
		if(this.getMileage() > 5000) {
			System.out.println("The owner," + ownerName + " would like to sell the " + this.color + " " + this.getName() + " at $" + (this.getCost() - 5000));
		}
	}
	
	
}


public class MilitaryVehicle extends Vehicle{
	private boolean isLoadWeapon;

	public MilitaryVehicle(boolean isLoadWeapon) {
		super("RV", 90000.0,"diesel" , 62, "land");
		this.isLoadWeapon = isLoadWeapon;
	}
	
	public void attack() {
		System.out.println("Military vehicle attacked");
	}
	public void defend() {
		System.out.println("Military vehicle defend");
	}

	public boolean isLoadWeapon() {
		return isLoadWeapon;
	}

	public void setLoadWeapon(boolean isLoadWeapon) {
		this.isLoadWeapon = isLoadWeapon;
	}
	
}


public class PublicTransport extends CivilianVehicle{
	private int registrationNumber;
	private int onboardingPeople;
	
	
	public PublicTransport(String name, String fuel, int maxSpeed, String transportationMedium,
			int capacity, int mileage, int registrationNumber, int onboardingPeople) {
		super(name, 0, fuel, maxSpeed, transportationMedium, capacity, mileage);
		this.registrationNumber = registrationNumber;
		this.onboardingPeople = onboardingPeople;
	}

	public void service() {
		System.out.println("Service start");
	}

	
	
	public int getOnboardingPeople() {
		return onboardingPeople;
	}


	public void addOnBoardingPeople(int onboardingPeople) {
		if(onboardingPeople > 0) {
			this.onboardingPeople += onboardingPeople;
			if(this.onboardingPeople > 100) {
				System.out.println("Maximum capacity is reached");
			}
		}
	}

	public int getRegistrationNumber() {
		return registrationNumber;
	}
	
}

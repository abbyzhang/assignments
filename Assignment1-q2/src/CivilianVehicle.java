
public class CivilianVehicle extends Vehicle {
	
	private int mileage;
	private int capacity;
	
public CivilianVehicle(String name, double cost, String fuel, int maxSpeed, String transportationMedium,
			int capacity, int mileage) {
		super(name, cost, fuel, maxSpeed, transportationMedium);
		this.capacity = capacity;
		this.mileage = mileage;
	}

	
public String buy() {
		return " bought " + this.getName() + " for " + this.getCost();
	}
public void sell() {
		System.out.println("Sold!");
	}

public int getMileage() {
		return mileage;
	}
public void setMileage(int mileage) {
		this.mileage = mileage;
	}
}

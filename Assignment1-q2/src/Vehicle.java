
public class Vehicle {
	private String name;
	private double cost;
	private String fuel;
	private int maxSpeed;
	private String transportationMedium;
	
	public Vehicle(String name, double cost, String fuel, int maxSpeed, String transportationMedium) {
		this.name = name;
		this.cost = cost;
		this.fuel = fuel;
		this.maxSpeed = maxSpeed;
		this.transportationMedium = transportationMedium;
	}
	public void start() {
		System.out.println(name + " started");
	}
	public void stop() {
		System.out.println(name + " stopped");
	}
	public void move() {
		System.out.println(name + " is moving");
	}
	public void accelerate() {
		System.out.println(name + " is accelerate");
	}
	public void brake() {
		System.out.println("Brake");
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public String getTransportationMedium() {
		return transportationMedium;
	}
	public void setTransportationMedium(String transportationMedium) {
		this.transportationMedium = transportationMedium;
	}

}
